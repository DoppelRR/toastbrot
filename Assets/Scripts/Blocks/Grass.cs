﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : Block
{
    // Start is called before the first frame update
    void Start()
    {
        blockType = "Grass";
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
