﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
    protected PlayerManager player;
    protected string blockType;
    protected GameManager gameManager;

    protected void Initialize() {
        player = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<PlayerManager>();
        gameManager = GameObject.FindGameObjectsWithTag("GameManager")[0].GetComponent<GameManager>();
    }

    // Start is called before the first frame update
    void Start() {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {


    }


    public void OnMouseOver() {
        if (gameManager.IsPaused())
            return;

        if (Input.GetButton("LeftClick")) {
            DestroyBlock();
            Debug.Log(this.getBlockType());
            player.AddToInventory(this.getBlockType());
        }
    }

    protected void DestroyBlock() {
        Destroy(gameObject);
    }

    public string getBlockType() {
        return blockType;
    }


    public enum Blocks {
        STONE, GRASS, WOOD, LEAVES
    }
}