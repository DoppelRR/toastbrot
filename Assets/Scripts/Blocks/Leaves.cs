﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Leaves : Block
{
    int decayChance = 1;

    // Start is called before the first frame update
    void Start()
    {
        blockType = "Leaves";
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, new Vector2(2, 2), 0);
        bool decay = true;
        foreach (Collider2D c in hits) {
            if (c.gameObject.name.Contains("Wood")) {
                decay = false;
                break;
            }
        }

        if (decay && Random.Range(0, 100) < decayChance) {
            DestroyBlock();
        }
    }
}
