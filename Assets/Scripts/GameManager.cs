﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private bool paused;

    // Start is called before the first frame update
    void Awake()
    {
        paused = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Pause() {
        paused = true;
    }

    public void Unpause() {
        paused = false;
    }

    public bool IsPaused() {
        return paused;
    }
}
