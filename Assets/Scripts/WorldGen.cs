﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen : MonoBehaviour {
    public int width;
    public int initialHeight;
    public int treeProbability;
    public GameObject player;
    public GameObject[] blocks;

    // Start is called before the first frame update
    void Start() {
        player.transform.position = new Vector2(0, initialHeight + 1.5f);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < initialHeight; y++) {
                GameObject.Instantiate(blocks[0], new Vector2(x, y), Quaternion.identity);
            }
            GameObject.Instantiate(blocks[1], new Vector2(x, initialHeight), Quaternion.identity);

            int tree = Random.Range(0, 100);
            if (tree < treeProbability) {
                int treeHeight = Random.Range(3, 6);
                for (int h = 1; h <= treeHeight; h++) {
                    GameObject.Instantiate(blocks[2], new Vector2(x, initialHeight + h), Quaternion.identity);
                }
                GameObject.Instantiate(blocks[3], new Vector2(x, initialHeight + treeHeight + 1), Quaternion.identity);
                GameObject.Instantiate(blocks[3], new Vector2(x - 1, initialHeight + treeHeight), Quaternion.identity);
                GameObject.Instantiate(blocks[3], new Vector2(x + 1, initialHeight + treeHeight), Quaternion.identity);
            }


            int r = Random.Range(0, 100);
            if (r < 20) {
                initialHeight--;
            } else if (r > 80) {
                initialHeight++;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
