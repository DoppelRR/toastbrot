﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    int health;
    public Slider healthbar;

    public Font font;
    public Canvas inventoryCanvas;
    private bool inventoryOpen;
    Dictionary<string, int> inventory;

    public GameObject gameMangerObject;
    GameManager gameManager;



    // Start is called before the first frame update
    void Start()
    {
        inventoryCanvas.gameObject.SetActive(false);
        inventoryOpen = false;

        inventory = new Dictionary<string, int>();

        gameManager = gameMangerObject.GetComponent<GameManager>();

        health = 100;
        healthbar.value = health;
    }

    // Update is called once per frame
    void Update() {
        healthbar.value = health;

        if (Input.GetButtonDown("Inventory")) {
            if (inventoryOpen) {
                CloseInventory();
            } else {
                OpenInventory();
            }
        }


    }

    public void Damage(int i) {
        health -= i;
    }

    public void OpenInventory() {
        gameManager.Pause();
        inventoryCanvas.gameObject.SetActive(true);
        inventoryOpen = true;
    }

    public void CloseInventory() {
        gameManager.Unpause();
        inventoryCanvas.gameObject.SetActive(false);
        inventoryOpen = false;
    }

    public void AddToInventory(string item) {
        int count;
        if (!inventory.TryGetValue(item, out count)) {
            GameObject textGameObject = new GameObject(item);
            textGameObject.transform.SetParent(inventoryCanvas.transform);
            Text text = textGameObject.AddComponent<Text>();
            text.font = font;
            text.fontSize = 20;

            RectTransform rt = textGameObject.GetComponent<RectTransform>();
            rt.localPosition = new Vector2(0f, inventoryCanvas.transform.childCount * -30f - 30f);
            rt.anchorMax = new Vector2(0.5f, 1f);
            rt.anchorMin = new Vector2(0.5f, 1f);
            rt.pivot = new Vector2(0.5f, 1f);
            count = 0;
        }
        count++;
        inventoryCanvas.transform.Find(item).gameObject.GetComponentInChildren<Text>().text = item + ": " + count;
        inventory[item] = count;
    }
}
